#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "DefaultMeshModelShaderProgram.hpp"
#include "Texture.hpp"

struct  Vertex{
	glm::vec3 position_;
	glm::vec3 normal_;
	glm::vec2 textureCoordinates_;

	Vertex() {}

	Vertex(const glm::vec3 &position, const glm::vec3 &normal, const glm::vec2 &textureCoordinates){
		position_           = position;
		normal_             = normal;
		textureCoordinates_ = textureCoordinates;
	}
};

class Mesh{
public:
	Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture *> textures);

	/*
	* Uses shaderPgm to update the shader's uniforms, then draws all elements.
	*/
	void render();

	void setModel(const glm::mat4 &model);
	void setView(const glm::mat4 &view);
	void setProjection(const glm::mat4 &projection);
	void setTextures(const std::vector<Texture *> textures);
	/*
	* Creates the vao, the vbo, and binds the mesh's data to the vbo.
	*/
	void init();

private:
	std::vector<Vertex> vertices_;
	std::vector<GLuint> indices_;
	std::vector<Texture *> textures_;
	GLuint vao_, vbo_, ebo_;

	DefaultMeshModelShaderProgram shaderPgm_;
	glm::mat4 model_;
	glm::mat4 projection_;
	glm::mat4 view_;
};