#include "Texture.hpp"

Texture::Texture(GLenum textureTarget, const char *textureFilePath){
	textureTarget_ = textureTarget;
	textureFilePath_ = textureFilePath;
	isLoaded_ = false;
}

void Texture::loadTexture(){
	int width, height;

	unsigned char* image = SOIL_load_image(textureFilePath_, &width, &height, 0, SOIL_LOAD_RGBA);
	if(image == NULL){
		std::cerr << "Error loading texture from file with SOIL: "<< textureFilePath_ << std::endl;
		exit(EXIT_FAILURE);
	}

	glGenTextures(1, &texture_);
	glBindTexture(textureTarget_, texture_);
	if(!glIsTexture(texture_)){
		std::cerr << "Error generating texture" << std::endl;
		exit(EXIT_FAILURE);
	}

	//might want to change GL_RGB to a GLenum if we want depth attachments
	glTexImage2D(textureTarget_, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(textureTarget_);

	SOIL_free_image_data(image);

	glTexParameterf(textureTarget_, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(textureTarget_, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(textureTarget_, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(textureTarget_, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


	glBindTexture(textureTarget_, 0);
	isLoaded_ = true;
}

void Texture::bindTexture(GLenum textureUnit){
	if(!isLoaded_){
		std::cerr<<"WARNING: binded a texture that was not loaded int texture unit "<<textureUnit<<std::endl;
	}
	glActiveTexture(textureUnit);
	glBindTexture(textureTarget_, texture_);
}

const char *Texture::getTextureFilePath(){
	return textureFilePath_;
}