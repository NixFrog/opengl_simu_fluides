#pragma once

//Standard
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

//Opengl
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SOIL/SOIL.h>
// Assimp
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.hpp"
#include "Texture.hpp"
#include "Utils.hpp"

class MeshModel
{
public:
	MeshModel();
	~MeshModel();

	/*
	* Renders the model.
	*/
	void render();

	/*
	* Loads the model from a file.
	* @param filePath path to the model's files from the executable.
	*/
	void loadModel(std::string filePath);

	/*
	* Initialize all of the model's meshes.
	*/
	void initMeshes();

	void setModel(const glm::mat4 &model);
	void setProjection(const glm::mat4 &projection);
	void setView(const glm::mat4 &view);
	void setAllTextures(const std::vector<Texture *> textures);

	void setPosition(const glm::vec3 position);
	void setUpVector(const glm::vec3 up);
	void offsetRotation(const float offset);
	void setRotation(const float rotation);
	void setScale(const glm::vec3 scale);

	glm::mat4 getModel() const;

private:
	/*
	* Process one of the model's scene's nodes.
	* Creates a mesh for each of the node's mesh, then generates the node's children's meshes.
	* @param node any of the model's node
	* @param scene the model's scene
	*/
	void processNode(aiNode *node, const aiScene *scene);

	/*
	* Creates a Mesh from an aiMesh and a scene.
	* Generates all of the mesh's vertices.
	* @param mesh any of the model's meshes
	* @param scene the model's scene
	* @return the corresponding Mesh
	*/
	Mesh *processMesh(aiMesh *mesh, const aiScene *scene);

	/*
	* Creates Textures of the correct type from a material.
	*/
	std::vector<Texture *> loadMaterialTextures(aiMaterial *material, aiTextureType textureType);

private:
	std::vector<Mesh *> meshes_;
	std::string directory_;
	std::vector<Texture *> texturesLoaded_;

	glm::vec3 position_;
	glm::vec3 up_;
	float rotation_;
	glm::vec3 scale_;
};