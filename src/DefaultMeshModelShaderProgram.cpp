#include "DefaultMeshModelShaderProgram.hpp"

DefaultMeshModelShaderProgram::DefaultMeshModelShaderProgram(){}

void DefaultMeshModelShaderProgram::init(){
	ShaderProgramManager::init();
	addShader(GL_VERTEX_SHADER,		"./src/shaders/ModelShaders/default_vshader.vs");
	addShader(GL_FRAGMENT_SHADER,	"./src/shaders/ModelShaders/default_fshader.fs");
	completeInitialization();

	modelLocation_      = getUniformLocation("model");
	projectionLocation_ = getUniformLocation("projection");
	viewLocation_       = getUniformLocation("view");
}

void DefaultMeshModelShaderProgram::setModel(const glm::mat4 model){
	glUniformMatrix4fv(modelLocation_, 1, GL_FALSE, glm::value_ptr(model));
}

void DefaultMeshModelShaderProgram::setProjection(const glm::mat4 projection){
	glUniformMatrix4fv(projectionLocation_, 1, GL_FALSE, glm::value_ptr(projection));
}

void DefaultMeshModelShaderProgram::setView(const glm::mat4 view){
	glUniformMatrix4fv(viewLocation_, 1, GL_FALSE, glm::value_ptr(view));
}