#include "ParticleSystem.hpp"

ParticleSystem::ParticleSystem():
	particleTexture_(GL_TEXTURE_2D, "resources/images/custom_ball.png")
{
	isInitialized_     = false;
	currentReadBuffer_ = 0;

	particleGeneratorPosition_ = glm::vec3(0.0f, 0.0f, 0.0f);
	particleVelocityMin_       = glm::vec3(-2, 0, -2);
	particleVelocityRange_     = glm::vec3(3, 10, 3) - particleVelocityMin_;
	gravityVector_             = glm::vec3(0, -10, 0);
	particleColor_             = glm::vec3(0.0f, 0.5f, 1.0f);
	particleLifetimeMin_       = 100.0f;
	particleLifetimeRange_     = 300.0f - particleLifetimeMin_;
	particleSize_              = 0.1f;
	nextGenerationTime_        = 0.02f;
	floorPosition_             = -5.0f;
	nbOfParticlesToGenerate_   = 100;

	offsetSpeed_ = 5.0f;
}

void ParticleSystem::init(){
	if(isInitialized_){
		std::cerr<<"WARNING: double initialization of ParticleSystem instance"<<std::endl;
	}

	updatePgm_.init();
	billboardingPgm_.init();

	glGenBuffers(2, particleBufferArray_);
	glGenVertexArrays(2, vaoBufferArray_);
	glGenTransformFeedbacks(1, &transformFeedbackBuffer_);
	glGenQueries(1, &query_);

	Particle initParticle;
	initParticle.type = PARTICLE_TYPE_GENERATOR;
	initParticle.color = glm::vec3(1.0,1.0,1.0);

	for(int i=0; i<2; i++){	
		glBindVertexArray(vaoBufferArray_[i]);
		glBindBuffer(GL_ARRAY_BUFFER, particleBufferArray_[i]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Particle)*MAX_PARTICLES_ON_SCENE, NULL, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Particle), &initParticle);

		for(int j=0; j<NUM_PARTICLE_ATTRIBUTES; j++){
			glEnableVertexAttribArray(i);
		}

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)0);  // Position
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)12); // Velocity
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)24); // Color
		glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)36); // Lifetime
		glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)40); // Size
		glVertexAttribPointer(5, 1, GL_INT,	  GL_FALSE, sizeof(Particle), (const GLvoid*)44); // Type

		glBindVertexArray(0);
	}
	currentReadBuffer_ = 0;
	nbOfParticles_     = 1;
	isInitialized_     = true;

	particleTexture_.loadTexture();
}

float ParticleSystem::getRandomFloat(float min, float add){
	float random = float(rand()%(100+1))/float(100);
	return min+add*random;
}

void ParticleSystem::update(float timePassed){
	if(!isInitialized_){
		std::cerr<<"Error: calling update on a non-initialized instance of ParticleSystem"<<std::endl;
		exit(EXIT_FAILURE);
	}

	updatePgm_.activate();
	
	updatePgm_.setParticleGeneratorPosition(particleGeneratorPosition_);
	updatePgm_.setGeneratedParticleGravityVector(gravityVector_);
	updatePgm_.setGeneratedParticleMinimumVelocity(particleVelocityMin_);
	updatePgm_.setGeneratedParticleVelocityRange(particleVelocityRange_);
	updatePgm_.setGeneratedParticleColor(particleColor_);
	updatePgm_.setGeneratedParticleSize(particleSize_);
	updatePgm_.setFloorPosition(floorPosition_);
	updatePgm_.setGeneratedParticleMinimumLife(particleLifetimeMin_);
	updatePgm_.setGeneratedParticleLifeRange(particleLifetimeRange_);
	updatePgm_.setTimePassed(timePassed);
	updatePgm_.setNumberOfParticlesToGenerate(0);

	elapsedTime_ += timePassed;

	if(elapsedTime_ > nextGenerationTime_){
		updatePgm_.setNumberOfParticlesToGenerate(nbOfParticlesToGenerate_);
		elapsedTime_ -= nextGenerationTime_;

		glm::vec3 randomSeed = glm::vec3(getRandomFloat(-10.0f, 20.0f), getRandomFloat(-10.0f, 20.0f), getRandomFloat(-10.0f, 20.0f));
		updatePgm_.setRandomSeed(randomSeed);
	}

	glEnable(GL_RASTERIZER_DISCARD);

	glBindVertexArray(vaoBufferArray_[currentReadBuffer_]);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transformFeedbackBuffer_);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);

		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particleBufferArray_[1 - currentReadBuffer_]);

		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query_);
		glBeginTransformFeedback(GL_POINTS);

		glDrawArrays(GL_POINTS, 0, nbOfParticles_);
	glEndTransformFeedback();

	glDisable(GL_RASTERIZER_DISCARD);
	glFlush();

	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
	glGetQueryObjectiv(query_, GL_QUERY_RESULT, &nbOfParticles_);

	currentReadBuffer_ = 1 - currentReadBuffer_;
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
}

void ParticleSystem::render(){
	if(!isInitialized_){
		std::cerr<<"Error: calling renderParticles on a non-initialized instance of ParticleSystem"<<std::endl;
		exit(EXIT_FAILURE);
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		glDepthMask(0);

			billboardingPgm_.activate();
			billboardingPgm_.setModel(model_);
			billboardingPgm_.setView(view_);
			billboardingPgm_.setProjection(projection_);
			particleTexture_.bindTexture(GL_TEXTURE0);
			billboardingPgm_.setSampler(0);
			
			glBindVertexArray(vaoBufferArray_[currentReadBuffer_]);
				glDisableVertexAttribArray(1);
				glDrawArrays(GL_POINTS, 0, nbOfParticles_);
			glBindVertexArray(0);
			
		glDepthMask(1);
	glDisable(GL_BLEND);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void ParticleSystem::setGeneratorProperties(glm::vec3 particleGeneratorPosition, glm::vec3 particleVelocityMin, glm::vec3 genVelocityMax, glm::vec3 gravityVector, glm::vec3 particleColor, float particleLifetimeMin, float genLifeMax, float particleSize, float every, int nbOfParticlesToGenerate){
	particleGeneratorPosition_ = particleGeneratorPosition;
	particleVelocityMin_       = particleVelocityMin;
	particleVelocityRange_     = genVelocityMax - particleVelocityMin;
	gravityVector_             = gravityVector;
	particleColor_             = particleColor;
	particleSize_              = particleSize;
	particleLifetimeMin_       = particleLifetimeMin;
	particleLifetimeRange_     = genLifeMax - particleLifetimeMin;
	nextGenerationTime_        = every;
	elapsedTime_               = 0.8f;
	nbOfParticlesToGenerate_   = nbOfParticlesToGenerate;
}

void ParticleSystem::processKeyboardInput(Direction direction, GLfloat timeElapsed){
	float dist = -(offsetSpeed_ * timeElapsed);
	if(direction == UP){
		offsetParticleGenerationDirection(glm::vec3(0.0f, dist, 0.0f));
	}
	else if(direction == DOWN){
		offsetParticleGenerationDirection(glm::vec3(0.0f, -dist, 0.0f));	
	}
	else if(direction == RIGHT){
		offsetParticleGenerationDirection(glm::vec3(-dist, 0.0f, 0.0f));
	}
	else if(direction == LEFT){
		offsetParticleGenerationDirection(glm::vec3(dist, 0.0f, 0.0f));
	}
	else if(direction == FORWARD){
		offsetParticleGenerationDirection(glm::vec3(0.0f, 0.0f, dist));
	}
	else if(direction == BACKWARD){
		offsetParticleGenerationDirection(glm::vec3(0.0f, 0.0f, -dist));
	}
}

int ParticleSystem::getNbOfParticles() const{
	return nbOfParticles_;
}

void ParticleSystem::setModel(glm::mat4 model){
	model_ = model;
}

void ParticleSystem::setView(glm::mat4 view){
	view_ = view;
}

void ParticleSystem::setProjection(glm::mat4 projection){
	projection_ = projection;
}

void ParticleSystem::setGeneratorPosition(glm::vec3 particleGeneratorPosition){
	particleGeneratorPosition_ = particleGeneratorPosition;
}

void ParticleSystem::setParticleVelocityMin(glm::vec3 particleVelocityMin){
	particleVelocityMin_ = particleVelocityMin;
}

void ParticleSystem::setParticleVelocityMax(glm::vec3 particleVelocityMax){
	particleVelocityRange_ = particleVelocityMax - particleVelocityMin_;
}

void ParticleSystem::setGravityVector(glm::vec3 gravityVector){
	gravityVector_ = gravityVector;
}

void ParticleSystem::setParticleColor(glm::vec3 color){
	particleColor_ = color;
}

void ParticleSystem::setParticleLifetimeMin(float particleLifetimeMin){
	particleLifetimeMin_ = particleLifetimeMin;
}

void ParticleSystem::setParticleLifetimeRange(float particleLifetimeRange){
	particleLifetimeRange_ = particleLifetimeRange;
}

void ParticleSystem::setParticleSize(float particleSize){
	particleSize_ = particleSize;
}

void ParticleSystem::setNumberOfParticlesToGenerate(unsigned int nbToGenerate){
	nbOfParticlesToGenerate_ = nbToGenerate;
}

void ParticleSystem::offsetParticleGenerationDirection(glm::vec3 offsetVector){
	particleVelocityMin_       += offsetVector;
	particleVelocityRange_     += offsetVector;
}