#include "ParticleScene.hpp"

ParticleScene::ParticleScene():
	camera_(glm::vec3(0.0f, 1.5f, 30.0f))
{
	lastMouseX_ = WIDTH/2;
	lastMouseY_ = HEIGHT/2;
}

ParticleScene::~ParticleScene(){}

void ParticleScene::run(){
	init();
	while(!glfwWindowShouldClose(window_)){
		render();
		checkErrors();
		glfwPollEvents();
	}
	glfwTerminate();
}

void ParticleScene::init(){
	startingTime_ = std::chrono::high_resolution_clock::now();

	initContext();
	initMeshes();
	initParticleSystem();
	skybox_.init();

	glPointSize(3.0);
	glEnable(GL_DEPTH_TEST);
	glfwSetCursorPos(window_, 0, 0);
}

void ParticleScene::initContext(){
	window_ = windowCreator_.createWindow("OpenGL");
	glfwMakeContextCurrent(window_);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		std::cerr << "Failed to initialize GLEW" << std::endl;
		exit(EXIT_FAILURE);
	}
	glGetError();	//sometimes glewInit generates GL_INVALID_ENUM because of glew experimental
}

void ParticleScene::initMeshes(){
	groundMeshModel_.loadModel("resources/objects/quad.obj");
	groundMeshModel_.setPosition(glm::vec3(1,1,5.1));
	groundMeshModel_.setUpVector(glm::vec3(1,0,0));
	groundMeshModel_.setRotation(90.0f);
	groundMeshModel_.setScale(glm::vec3(80,80,80));
	meshModels_.push_back(&groundMeshModel_);

	Texture *texture = new Texture(GL_TEXTURE_2D, "resources/images/ground.jpg");
	texture->loadTexture();
	std::vector<Texture *> v;
	v.push_back(texture);
	groundMeshModel_.setAllTextures(v);

	appleMeshModel_.loadModel("resources/objects/apple.obj");
	appleMeshModel_.setPosition(glm::vec3(0.5f,0,0));
	meshModels_.push_back(&appleMeshModel_);

	soldierMeshModel_.loadModel("resources/objects/nanosuit/nanosuit.obj");
	soldierMeshModel_.setPosition(glm::vec3(10,-5,-5));
	meshModels_.push_back(&soldierMeshModel_);

	soldierMeshModel2_.loadModel("resources/objects/nanosuit/nanosuit.obj");
	soldierMeshModel2_.setPosition(glm::vec3(-10,-5,-5));
	meshModels_.push_back(&soldierMeshModel2_);
}

void ParticleScene::initParticleSystem(){
	waterParticleSystem_.init();
	particleSystems_.push_back(&waterParticleSystem_);

	fireParticleSystem_.init();
	particleSystems_.push_back(&fireParticleSystem_);

	smokeParticleSystem_.init();
	particleSystems_.push_back(&smokeParticleSystem_);

	fireParticleSystem_.setGeneratorPosition(glm::vec3(5.0f,0.0f,.0f));
	fireParticleSystem_.setParticleVelocityMin(glm::vec3(-2, 0, -2));
	fireParticleSystem_.setParticleVelocityMax(glm::vec3(2, 3, 2));
	fireParticleSystem_.setGravityVector(glm::vec3(0.0f, 1.0f, 0.0f));
	fireParticleSystem_.setParticleColor(glm::vec3(1.0f, 0.5f, 0.0f));
	fireParticleSystem_.setParticleLifetimeMin(0.1f);
	fireParticleSystem_.setParticleLifetimeRange(1.0f);
	fireParticleSystem_.setNumberOfParticlesToGenerate(100);
	fireParticleSystem_.setParticleSize(0.2f);

	smokeParticleSystem_.setGeneratorPosition(glm::vec3(-5.0f,0.0f,.0f));
	smokeParticleSystem_.setParticleVelocityMin(glm::vec3(-0.2f, -0.2, -0.2f));
	smokeParticleSystem_.setParticleVelocityMax(glm::vec3( 0.2f,  0.2,  0.2f));
	smokeParticleSystem_.setGravityVector(glm::vec3(0.0f, 0.5f, 0.0f));
	smokeParticleSystem_.setParticleColor(glm::vec3(0.05f, 0.05f, 0.05f));
	smokeParticleSystem_.setParticleLifetimeMin(4.0f);
	smokeParticleSystem_.setParticleLifetimeRange(5.0f);
	smokeParticleSystem_.setNumberOfParticlesToGenerate(100);
	smokeParticleSystem_.setParticleSize(0.2f);
}

void ParticleScene::checkErrors(){
	GLenum error = glGetError();
	if(error != GL_NO_ERROR){
		std::cerr<<std::endl;
		while(error != GL_NO_ERROR){
			std::cerr<<"OpenGL error at runtime: "<<error<< "\t"<<gluErrorString(error)<<std::endl;
			error = glGetError();
		}
		std::cerr<<std::endl;
	}
}

void ParticleScene::render(){
	// Clear the screen to dark blue
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.1f, 0.1f, 0.2f, 1.0f);

	auto currentTime = std::chrono::high_resolution_clock::now();
	float timeElapsed = std::chrono::duration_cast<std::chrono::duration<float>>(currentTime - startingTime_).count();
	startingTime_ = std::chrono::high_resolution_clock::now();

	//Draw Meshes
	for(MeshModel *mm : meshModels_){
		mm->setProjection(camera_.getProjectionMatrix(WIDTH, HEIGHT));
		mm->setView(camera_.getViewMatrix());
		mm->render();
	}
	
	//Draw skybox
	skybox_.setView(camera_.getViewMatrix());
	skybox_.setProjection(camera_.getProjectionMatrix(WIDTH, HEIGHT));
	skybox_.render();

	//Draw particles
	for(ParticleSystem *ps : particleSystems_){
		ps->setModel(glm::rotate(glm::mat4(), glm::radians(0.0f), glm::vec3(0,1,0)));
		ps->setView(camera_.getViewMatrix());
		ps->setProjection(camera_.getProjectionMatrix(WIDTH, HEIGHT));
		ps->update(timeElapsed);
		ps->render();
	}

	computeInput(timeElapsed);


	glBindVertexArray(0);
	glfwSwapBuffers(window_);
}

void ParticleScene::computeInput(float timeElapsed){
	computeCameraInput(timeElapsed);
	computeFountainInput(timeElapsed);
}

void ParticleScene::computeCameraInput(float timeElapsed){
	if(glfwGetKey(window_, GLFW_KEY_DOWN)){
		camera_.processKeyboardInput(BACKWARD, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_UP)){
		camera_.processKeyboardInput(FORWARD, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_RIGHT)){
		camera_.processKeyboardInput(RIGHT, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_LEFT)){
		camera_.processKeyboardInput(LEFT, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_PAGE_UP)){
		camera_.processKeyboardInput(UP, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_PAGE_DOWN)){
		camera_.processKeyboardInput(DOWN, timeElapsed);
	}

	int state = glfwGetMouseButton(window_, GLFW_MOUSE_BUTTON_LEFT);
	if(state == GLFW_PRESS){
		double mouseX, mouseY;
		glfwGetCursorPos(window_, &mouseX, &mouseY);

		GLfloat xOffset = mouseX - lastMouseX_;
		GLfloat yOffset = mouseY - lastMouseY_;
		lastMouseX_ = mouseX;
		lastMouseY_ = mouseY;
		camera_.processMouseMovement(xOffset, yOffset);
	}

	state = glfwGetMouseButton(window_, GLFW_MOUSE_BUTTON_RIGHT);
	if(state == GLFW_PRESS){
		double mouseX, mouseY;
		glfwGetCursorPos(window_, &mouseX, &mouseY);
		waterParticleSystem_.setGeneratorPosition(glm::vec3(mouseX, 0.0f, mouseY));
	}
}

void ParticleScene::computeFountainInput(float timeElapsed){
	if(glfwGetKey(window_, GLFW_KEY_S)){
		waterParticleSystem_.processKeyboardInput(UP, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_W)){
		waterParticleSystem_.processKeyboardInput(DOWN, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_D)){
		waterParticleSystem_.processKeyboardInput(RIGHT, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_A)){
		waterParticleSystem_.processKeyboardInput(LEFT, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_Q)){
		waterParticleSystem_.processKeyboardInput(FORWARD, timeElapsed);
	}
	if(glfwGetKey(window_, GLFW_KEY_Z)){
		waterParticleSystem_.processKeyboardInput(BACKWARD, timeElapsed);
	}
}