#include "MeshModel.hpp"

MeshModel::MeshModel(){
	position_ = glm::vec3(0, 0, 0);
	up_ = glm::vec3(0, 1, 0);
	rotation_ = 0.0f;
	scale_ = glm::vec3(1.0f, 1.0f, 1.0f);
}

MeshModel::~MeshModel(){
	for(Mesh *m : meshes_){
		SAFE_DELETE(m);
	}

	for(Texture *t : texturesLoaded_){
		SAFE_DELETE(t);
	}
}

void MeshModel::initMeshes(){
	for(Mesh *m: meshes_){
		m->init();
	}
}

void MeshModel::render(){
	setModel(getModel());
	for(Mesh *m: meshes_){
		m->render();
	}
}

void MeshModel::loadModel(std::string filePath){
	Assimp::Importer importer;

	const aiScene *scene = importer.ReadFile(filePath, aiProcess_Triangulate      |
													   aiProcess_GenSmoothNormals |
													   aiProcess_FlipUVs		  |
													   aiProcess_CalcTangentSpace );

	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode){
		std::cerr<<"Error with Assimp parsing "<<filePath<<": "<<importer.GetErrorString()<<std::endl;
		exit(EXIT_FAILURE);
	}

	directory_ = filePath.substr(0, filePath.find_last_of('/'));
	processNode(scene->mRootNode, scene);
	initMeshes();
}

void MeshModel::processNode(aiNode *node, const aiScene *scene){
	for(unsigned int i=0; i<node->mNumMeshes; i++){
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		meshes_.push_back(processMesh(mesh, scene));
	}

	for(unsigned int i=0; i<node->mNumChildren; i++){
		processNode(node->mChildren[i], scene);
	}
}

Mesh *MeshModel::processMesh(aiMesh *mesh, const aiScene *scene){
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture*> textures;
	
	const aiVector3D vectorZero(0.0f, 0.0f, 0.0f);

	for(GLuint i=0; i<mesh->mNumVertices; i++){
		const aiVector3D* pos    = &(mesh->mVertices[i]);
		const aiVector3D* normal = &(mesh->mNormals[i]);
		const aiVector3D* textureCoordinates;
		
		if(mesh->HasTextureCoords(0)){
			textureCoordinates = &(mesh->mTextureCoords[0][i]);
		}
		else{
			textureCoordinates = &vectorZero;
		}

		Vertex vertex(glm::vec3(pos->x, pos->y, pos->z),
					  glm::vec3(normal->x, normal->y, normal->z),
					  glm::vec2(textureCoordinates->x, textureCoordinates->y)
					);

		vertices.push_back(vertex);
	}

	for(unsigned long i=0; i<mesh->mNumFaces; i++){
		aiFace face= mesh->mFaces[i];
		for(GLuint j=0; j<face.mNumIndices; j++){
			indices.push_back(face.mIndices[j]);
		}
	}


	aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
	
	// Diffuse maps
	std::vector<Texture *> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE);
	textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
	// Specular maps
	std::vector<Texture *> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR);
	textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

	return new Mesh(vertices, indices, textures);
}

std::vector<Texture *> MeshModel::loadMaterialTextures(aiMaterial *material, aiTextureType textureType){
	std::vector<Texture *> textures;

	for(unsigned int i=0; i<material->GetTextureCount(textureType); i++){
		aiString endOfTexturePath;
		if(material->GetTexture(textureType, i, &endOfTexturePath) != AI_SUCCESS){
			std::cerr<<"Error with Assimp loading material textures"<<std::endl;
			exit(EXIT_FAILURE);
		}

		bool skip = false;
		for(unsigned int j=0; j<texturesLoaded_.size(); j++){
			if(texturesLoaded_[j]->getTextureFilePath() == endOfTexturePath.data){
				textures.push_back(texturesLoaded_[j]);
				skip = true;
				break;
			}
		}
		if(!skip){
			std::string filename = directory_ + '/' + endOfTexturePath.data;

			Texture *texture = new Texture(GL_TEXTURE_2D, filename.c_str());
			texture->loadTexture();
			textures.push_back(texture);
			texturesLoaded_.push_back(texture);
		}
	}
	return textures;
}

void MeshModel::setModel(const glm::mat4 &model){
	for(Mesh *m :meshes_){
		m->setModel(model);
	}
}

void MeshModel::setProjection(const glm::mat4 &projection){
	for(Mesh *m : meshes_){
		m->setProjection(projection);
	}
}

void MeshModel::setView(const glm::mat4 &view){
	for(Mesh *m : meshes_){
		m->setView(view);
	}
}

void MeshModel::setPosition(const glm::vec3 position){
	position_ = position;
}

void MeshModel::setUpVector(const glm::vec3 up){
	up_ = up;
}

void MeshModel::offsetRotation(const float offset){
	rotation_ += offset;
}

void MeshModel::setRotation(const float rotation){
	rotation_ = rotation;
}

void MeshModel::setScale(const glm::vec3 scale){
	scale_ = scale;
}

void MeshModel::setAllTextures(const std::vector<Texture *> textures){
	for(Mesh *m : meshes_){
		m->setTextures(textures);
	}
}

glm::mat4 MeshModel::getModel() const{
	return glm::scale(glm::translate( glm::rotate( glm::mat4(), glm::radians(rotation_), glm::vec3(up_)), glm::vec3(position_)), glm::vec3(scale_));
}
