#include "Skybox.hpp"

Skybox::Skybox(){
	faces_.push_back("resources/skybox/right.jpg");
	faces_.push_back("resources/skybox/left.jpg");
	faces_.push_back("resources/skybox/top.jpg");
	faces_.push_back("resources/skybox/bottom.jpg");
	faces_.push_back("resources/skybox/back.jpg");
	faces_.push_back("resources/skybox/front.jpg");
	isInitialized_ = false;
}

Skybox::~Skybox(){
	glDeleteVertexArrays(1, &vao_);
}

void Skybox::init(){
	if(isInitialized_){
		std::cerr<<"WARNING: initializing Skybox twice"<<std::endl;
	}
	GLfloat skyboxVertices[] = {    
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f
	};

	glGenVertexArrays(1, &vao_);
	glGenBuffers(1, &vbo_);

	glBindVertexArray(vao_);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_);

	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);

	glBindVertexArray(0);

	cubeMapTexture_ = loadCubemap();

	shaderPgm_.init();
	isInitialized_ = true;
}

GLuint Skybox::loadCubemap(){
	GLuint textureID;
	glGenTextures(1, &textureID);

	int width,height;
	unsigned char* image;

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	if(!glIsTexture(textureID)){
		std::cerr << "Error in skybox generating texture" << std::endl;
		exit(EXIT_FAILURE);
	}

	for(unsigned int i=0; i<faces_.size(); i++){
		image = SOIL_load_image(faces_[i], &width, &height, 0, SOIL_LOAD_RGB);
		if(image == NULL){
			std::cerr << "Error in skybox loading texture from file with SOIL: "<< faces_[i] << std::endl;
			exit(EXIT_FAILURE);
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		SOIL_free_image_data(image);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	
	return textureID;
}

void Skybox::render(){
	if(!isInitialized_){
		std::cerr<<"Error: rendering uninitialized skybox"<<std::endl;
		exit(EXIT_FAILURE);
	}
	glDepthFunc(GL_LEQUAL);
	shaderPgm_.activate();

	shaderPgm_.setView(view_);
	shaderPgm_.setProjection(projection_);

	glActiveTexture(GL_TEXTURE0);
	shaderPgm_.setSkybox(0);

	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture_);
	
	glBindVertexArray(vao_);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	
	glDepthFunc(GL_LESS);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Skybox::setView(glm::mat4 viewMatrix){
	view_ = glm::mat4(glm::mat3(viewMatrix));
}

void Skybox::setProjection(glm::mat4 projectionMatrix){
	projection_ = projectionMatrix;
}