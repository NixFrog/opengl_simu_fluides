#pragma once

#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderProgramManager.hpp"

class DefaultMeshModelShaderProgram : public ShaderProgramManager
{
// functions
public:
	DefaultMeshModelShaderProgram();
	virtual void init();

// setters
public:
	void setModel(const glm::mat4 model);
	void setProjection(const glm::mat4 projection);
	void setView(const glm::mat4 view);
	
private:
	GLuint modelLocation_;
	GLuint projectionLocation_;
	GLuint viewLocation_;
};