#include "SkyboxShaderProgram.hpp"

SkyboxShaderProgram::SkyboxShaderProgram(){}

void SkyboxShaderProgram::init(){
	ShaderProgramManager::init();
	addShader(GL_VERTEX_SHADER,		"./src/shaders/ModelShaders/skybox_vshader.vs");
	addShader(GL_FRAGMENT_SHADER,	"./src/shaders/ModelShaders/skybox_fshader.fs");
	completeInitialization();

	viewLocation_       = getUniformLocation("view");
	projectionLoaction_ = getUniformLocation("projection");
	skyboxLocation_     = getUniformLocation("skybox");
}

void SkyboxShaderProgram::setView(const glm::mat4 view){
	glUniformMatrix4fv(viewLocation_, 1, GL_FALSE, glm::value_ptr(view));
}

void SkyboxShaderProgram::setProjection(const glm::mat4 projection){
	glUniformMatrix4fv(projectionLoaction_, 1, GL_FALSE, glm::value_ptr(projection));
}

void SkyboxShaderProgram::setSkybox(unsigned int skybox){
	glUniform1i(skyboxLocation_, skybox);
}