#version 330 

// DEFAULT INPUT & OUTPUT
layout(points) in;
layout(points) out;
layout(max_vertices = 40) out;

// INPUT
in vec3  vPositionPass[];
in vec3  vVelocityPass[];
in vec3  vColorPass[];
in float fLifeTimePass[];
in float fSizePass[];
in int   iTypePass[];

// OUTPUT
out vec3  vPositionOut;
out vec3  vVelocityOut;
out vec3  vColorOut;
out float fLifeTimeOut;
out float fSizeOut;
out int   iTypeOut;

// UNIFORMS
uniform vec3 vGenPosition;
uniform vec3 vGenGravityVector;
uniform vec3 vGenVelocityMin;
uniform vec3 vGenVelocityRange;

uniform vec3  vColor;
uniform float fParticleSize;
uniform float fFloorPosition;

uniform float fLifetimeMin, fLifetimeRange;
uniform float fTimePassed;

uniform int iNumToGenerate;
uniform vec3 vRandomSeed;

// LOCAL
vec3 vLocalSeed;

/*****************************************************************************************************************************/


/* 
* Calculates random floating integer between 0 and 1 according to a local seed
*  @return a random floatting integer between 0 and 1
*/
float randZeroOne(){
	uint n = floatBitsToUint(vLocalSeed.y * 214013.0 + vLocalSeed.x * 2531011.0 + vLocalSeed.z * 141251.0);
	n = n * (n * n * 15731u + 789221u);
	n = (n >> 9u) | 0x3F800000u;

	float fRes =  2.0 - uintBitsToFloat(n);
	vLocalSeed = vec3(vLocalSeed.x + 147158.0 * fRes, vLocalSeed.y*fRes  + 415161.0 * fRes, vLocalSeed.z + 324154.0*fRes);
	return fRes;
}

void main(){ 
	vLocalSeed = vRandomSeed;

	vPositionOut = vPositionPass[0];
	vVelocityOut = vVelocityPass[0];
	
	if(iTypePass[0] != 0){
		vPositionOut += vVelocityOut*fTimePassed;
		if(vPositionOut.y > fFloorPosition){
			vVelocityOut += vGenGravityVector*fTimePassed;
		}
		else{
			vVelocityOut += vec3(-vVelocityOut.x/1.2, -vVelocityOut.y , -vVelocityOut.z/1.2);
			if(vVelocityOut.x < 0.5){
				vVelocityOut.x = 0;
			}
			if(vVelocityOut.y < 0.5){
				vVelocityOut.y = 0;
			}
			if(vVelocityOut.z < 0.5){
				vVelocityOut.z = 0;
			}
		}
	}

	vColorOut    = vColorPass[0];
	fLifeTimeOut = fLifeTimePass[0]-fTimePassed;
	fSizeOut     = fSizePass[0];
	iTypeOut     = iTypePass[0];

	if(iTypeOut == 0){
		EmitVertex();
		EndPrimitive();

		for(int i = 0; i < iNumToGenerate; i++){
			vPositionOut = vec3(vGenPosition.x + randZeroOne(), vGenPosition.y + randZeroOne(), vGenPosition.z + randZeroOne());
			vVelocityOut = vGenVelocityMin + vec3(vGenVelocityRange.x * randZeroOne(), vGenVelocityRange.y * randZeroOne(), vGenVelocityRange.z * randZeroOne());
			vColorOut    = vColor;
			fLifeTimeOut = fLifetimeMin+fLifetimeRange*randZeroOne();
			fSizeOut     = fParticleSize;
			iTypeOut     = 1;
			EmitVertex();
			EndPrimitive();
		}
	}
	else if(fLifeTimeOut > 0.0){
		EmitVertex();
		EndPrimitive();
	}
	EndPrimitive();	
}