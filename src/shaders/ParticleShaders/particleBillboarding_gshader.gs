#version 330 core

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;

in vec3 vColorPass[];
in float fLifeTimePass[];
in float fSizePass[];
in int iTypePass[];

smooth out vec2 vTexCoord;
flat out vec4 vColorPart;

void main(){
	if(iTypePass[0] != 0){
		vColorPart = vec4(vColorPass[0], fLifeTimePass[0]);

		vec4 eyePos = gl_in[0].gl_Position;
		eyePos.xy	+= fSizePass[0] * (vec2(0.0, 1.0) - vec2(0.5));
		gl_Position = eyePos;
		vTexCoord	= vec2(0.0, 1.0);
		EmitVertex();

		eyePos = gl_in[0].gl_Position;
		eyePos.xy	+= fSizePass[0] * (vec2(0.0, 0.0) - vec2(0.5));
		gl_Position = eyePos;
		vTexCoord	= vec2(0.0, 0.0);
		EmitVertex();

		eyePos = gl_in[0].gl_Position;
		eyePos.xy	+= fSizePass[0] * (vec2(1.0, 1.0) - vec2(0.5));
		gl_Position = eyePos;
		vTexCoord	= vec2(1.0, 1.0);
		EmitVertex();

		eyePos = gl_in[0].gl_Position;
		eyePos.xy	+= fSizePass[0] * (vec2(1.0, 0.0) - vec2(0.5));
		gl_Position = eyePos;
		vTexCoord	= vec2(1.0, 0.0);
		EmitVertex();

		EndPrimitive();
	}
	EndPrimitive();
}