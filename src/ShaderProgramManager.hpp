#pragma once

#include <iostream>
#include <fstream>
#include <streambuf>
#include <list>
#include <vector>

#include <GL/glew.h>

#include "Utils.hpp"

class ShaderProgramManager
{
public:
	ShaderProgramManager();

	/*
	* Destructor.
	* Deletes the shaders that have been added to the program, then the program itself.
	*/
	virtual ~ShaderProgramManager();

	/*
	* Creates the shader program.
	*/
	virtual void init();

	/*
	* Uses the shader.
	* Can return a warning if the program wasn't properly initiated
	*/
	void activate();

protected:
	/*
	* Adds a shader from a text file to the program.
	* This does not complete the shader program, and completeInitialization() should be
	* called later on.
	* @param type Type of the shader to be added
	* @param shaderFilePath path to the shader from the bin file
	*/
	void addShader(GLenum type, std::string shaderFilePath);
	
	/*
	* Checks that a single shader compiled correctly.
	* Writes the compilation results in a char buffer.
	* @param shader uint of the shader's adress
	* @param logFileName name of the logfile where the compilation results will be writen
	*/
	void checkShaderCompilation(GLuint shaderUint, std::string logFileName);

	/*
	* Finished the initialization.
	* Has to be called afetr init()
	*/
	void completeInitialization();

	GLint getUniformLocation(const char* uniformName);
	GLint getShaderProgramParameter(GLint parameter);

private:
	/*
	* Returns a text file as a std::string.
	* @param filePath path to the text file
	* @result an std::string of the file's content
	*/
	std::string readTextFile(std::string shaderFilePath);

	/*
	* Converts a shader path into a log file name.
	* @param filePath path to the shader file
	* @returns a string of the log file's name
	*/
	std::string findLogFileFromPath(std::string filePath);

	/*
	* Writes the content of a buffer into a .log file.
	* @param buffer buffer of chars containing the logs
	* @param logFilePath path of the .log file to write in
	*/
	void writeCompilationResultsInFile(char *buffer, std::string logFilePath);

	/*
	* Checks that the shader program's status is correct.
	* Writes the compilation results in a char buffer.
	* @param logFileName name of the logfile where the compilation results will be writen
	*/
	void checkShaderProgramStatus(std::string logFileName);

	/*
	* Checks that the shader program was linked correctly.
	* Writes the compilation results in a char buffer.
	* @param logFileName name of the logfile where the compilation results will be writen
	*/
	void checkShaderProgramLinking(std::string logFileName);

protected:
	GLuint shaderProgram_;

private:
	std::vector<GLuint> shadersVector_;
	bool isInitialized_;
};
