#include "ParticleBillboardingShaderProgram.hpp"

ParticleBillboardingShaderProgram::ParticleBillboardingShaderProgram(){}

void ParticleBillboardingShaderProgram::init(){
	ShaderProgramManager::init();
	addShader(GL_VERTEX_SHADER,		"./src/shaders/ParticleShaders/particleBillboarding_vshader.vs");
	addShader(GL_GEOMETRY_SHADER,	"./src/shaders/ParticleShaders/particleBillboarding_gshader.gs");
	addShader(GL_FRAGMENT_SHADER,	"./src/shaders/ParticleShaders/particleBillboarding_fshader.fs");
	completeInitialization();

	projectionLocation_   = getUniformLocation("projection");
	viewLocation_         = getUniformLocation("view");
	modelLocation_        = getUniformLocation("model");
	samplerLocation_      = getUniformLocation("gSampler");
}

void ParticleBillboardingShaderProgram::setProjection(const glm::mat4 projection){
	glUniformMatrix4fv(projectionLocation_, 1, GL_FALSE, glm::value_ptr(projection));
}

void ParticleBillboardingShaderProgram::setView(const glm::mat4 view){
	glUniformMatrix4fv(viewLocation_, 1, GL_FALSE, glm::value_ptr(view));
}

void ParticleBillboardingShaderProgram::setModel(const glm::mat4 model){
	glUniformMatrix4fv(modelLocation_, 1, GL_FALSE, glm::value_ptr(model));
}

void ParticleBillboardingShaderProgram::setSampler(unsigned int textureUnit){
	glUniform1i(samplerLocation_, textureUnit);
}