#include "WindowCreator.hpp"

WindowCreator::WindowCreator(){}

GLFWwindow *WindowCreator::createWindow(char const *title){
	glfwSetErrorCallback(errorCallback);

	if (!glfwInit()) {
		std::cerr << "Failed to initialize GLFW" << std::endl;
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	//Context should at least support OpenGL 3.2
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow *window_ = glfwCreateWindow(WIDTH, HEIGHT, title, nullptr, nullptr);
	if (window_ == NULL) {
		std::cerr << "Failed to open GLFW window" << std::endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window_, keyCallback);

	return window_;
}

void WindowCreator::errorCallback(int error, const char *errorDescription){
	std::cerr << "Error #" << error << "\t" << errorDescription << std::endl;
}

void WindowCreator::keyCallback(GLFWwindow* window_, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window_, GL_TRUE);
	}
}