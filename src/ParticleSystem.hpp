#pragma once

//OpenGL
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

//Self
#include "ParticleUpdateShaderProgram.hpp"
#include "ParticleBillboardingShaderProgram.hpp"
#include "Texture.hpp"

#include "Utils.hpp"

#define NUM_PARTICLE_ATTRIBUTES 6
#define MAX_PARTICLES_ON_SCENE 1000000

#define PARTICLE_TYPE_GENERATOR 0
#define PARTICLE_TYPE_NORMAL 1

struct Particle{
public:
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec3 color;
	float lifeTime;
	float size;
	int type;
};

class ParticleSystem
{
public:
	ParticleSystem();

	/*
	* Generates the vao, vbo, tfb and query.
	* Initializes the vbo with the generator particle.
	*/
	void init();

	/*
	* Updates all particles in the scene with the tfb program.
	*/
	void update(float timePassed);

	/*
	* renders all particles in the scene with the billboarding program
	*/
	void render();

	void clearParticles();
	bool releaseParticleSystem();

	void processKeyboardInput(Direction direction, GLfloat timeElapsed);

	int getNbOfParticles() const;

	void setGeneratorProperties(glm::vec3 genPosition, glm::vec3 particleVelocityMin, glm::vec3 particleVelocityMax, glm::vec3 genGravityVector, glm::vec3 genColor, float genLifeMin, float genLifeMax, float genSize, float every, int nbToGenerate);
	
	void setModel(glm::mat4 model);
	void setView(glm::mat4 view);
	void setProjection(glm::mat4 projection);

	void setGeneratorPosition(glm::vec3 genPosition);
	void setParticleVelocityMin(glm::vec3 particleVelocityMin);
	void setParticleVelocityMax(glm::vec3 particleVelocityMax);
	void setGravityVector(glm::vec3 gravityVector_);
	void setParticleColor(glm::vec3 color);
	void setParticleLifetimeMin(float particleLifetimeMin);
	void setParticleLifetimeRange(float particleLifetimeRange);
	void setParticleSize(float particleSize);
	void setNumberOfParticlesToGenerate(unsigned int nbToGenerate);

	void offsetParticleGenerationDirection(glm::vec3 offsetVector);

private:
	float getRandomFloat(float min, float add);

private:
	bool isInitialized_;
	
	GLuint transformFeedbackBuffer_;
	GLuint particleBufferArray_[2];
	GLuint vaoBufferArray_[2];
	GLuint query_;

	unsigned int currentReadBuffer_;
	int nbOfParticles_;
	
	glm::mat4 model_;
	glm::mat4 view_;
	glm::mat4 projection_;

	Texture particleTexture_;

	float elapsedTime_;

	glm::vec3 particleGeneratorPosition_;
	glm::vec3 particleVelocityMin_;
	glm::vec3 particleVelocityRange_;
	glm::vec3 gravityVector_;
	glm::vec3 particleColor_;

	float particleLifetimeMin_;
	float particleLifetimeRange_;
	float particleSize_;
	float nextGenerationTime_;
	float floorPosition_;
	
	unsigned int nbOfParticlesToGenerate_;

	ParticleBillboardingShaderProgram billboardingPgm_;
	ParticleUpdateShaderProgram updatePgm_;

	float offsetSpeed_;
};