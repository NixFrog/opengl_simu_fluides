#pragma once

#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderProgramManager.hpp"

class SkyboxShaderProgram : public ShaderProgramManager
{
// functions
public:
	SkyboxShaderProgram();
	virtual void init();

// setters
public:
	void setView(const glm::mat4 view);
	void setProjection(const glm::mat4 projection);
	void setSkybox(unsigned int skybox);
	
private:
	GLuint viewLocation_;
	GLuint projectionLoaction_;
	GLuint skyboxLocation_;
};