#pragma once

#include "ShaderProgramManager.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#define NB_PARTICLE_ATTRIBUTES 6

class ParticleUpdateShaderProgram : public ShaderProgramManager
{

// functions
public:
	ParticleUpdateShaderProgram();
	virtual void init();    

// setters	
public:
	void setParticleGeneratorPosition(const glm::vec3 generatorParticle);
	void setGeneratedParticleGravityVector(const glm::vec3 generatedParticleGravityVector);
	void setGeneratedParticleMinimumVelocity(const glm::vec3 generatedParticleMinimumVeolocity);
	void setGeneratedParticleVelocityRange(const glm::vec3 generatedParticleVelocityRange);
	void setGeneratedParticleColor(const glm::vec3 generatedParticleColor);
	void setGeneratedParticleSize(float generatedParticleSize);
	void setGeneratedParticleMinimumLife(float generatedParticleMinimumLife);
	void setGeneratedParticleLifeRange(float generatedParticleLifeRange);
	void setTimePassed(float timePassed);
	void setNumberOfParticlesToGenerate(int numberOfParticlesToGenerate);
	void setRandomSeed(const glm::vec3 randomSeed);
	void setFloorPosition(float floorPosition);

// getters
public:
	int getSizeOfFeedbackVaryingsArray();
	
private:
	GLuint particleGeneratorPositionLocation_;
	GLuint generatedParticleGravityVectorLocation_;
	GLuint generatedParticleMinimumVelocityLocation_;
	GLuint generatedParticleVelocityRangeLocation_;
	
	GLuint generatedParticleColorLocation_;
	GLuint generatedParticleSizeLocation_;
	
	GLuint generatedParticleMinimumLifeLocation_;
	GLuint generatedParticleLifeRangeLocation_;

	GLuint timePassedLocation_;
	GLuint numberOfParticlesToGenerateLocation_;
	GLuint randomSeedLocation_;

	GLuint floorPositionLocation_;
};