#pragma once

//Standard
#include <iostream>
#include <fstream>
#include <streambuf>
#include <ctime>
#include <chrono>
#include <vector>

// should not define GLEW_STATIC as it has been added to the Makefile
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

//headers
#include "WindowCreator.hpp"
#include "ParticleSystem.hpp"
#include "Camera.hpp"
#include "MeshModel.hpp"
#include "DefaultMeshModelShaderProgram.hpp"
#include "Skybox.hpp"
#include "Texture.hpp"

class ParticleScene
{
public:
	ParticleScene();
	~ParticleScene();

	void run();

private:

	/*
	* Create the context, initialize all of the scene's objects, centers the cursor, and sets OpenGL properties.
	*/
	void init();

	/*
	* Initializes OpenGL's context.
	*/
	void initContext();

	/*
	* Initializes the scene's Meshes.
	*/
	void initMeshes();

	/*
	* Initializes the scene's ParticleSystems
	*/
	void initParticleSystem();

	/*
	* Compute any input received by the GLFWindow
	*/
	void computeInput(float timeElapsed);

	/*
	* Check for standard OpenGL error
	*/
	void checkErrors();

	/*
	* Clears the scene, update the scene's objects, renders them, and treats user input.
	*/
	void render();

private:
	/*
	* Computes input destined to be treated by the Camera
	*/
	void computeCameraInput(float timeElapsed);

	/*
	* Computes inputs destined to be treated by the fountain.
	*/
	void computeFountainInput(float timeElapsed);

	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

private:
	WindowCreator windowCreator_;
	GLFWwindow *window_;
	
	Camera camera_;
	
	ParticleSystem waterParticleSystem_;
	ParticleSystem fireParticleSystem_;
	ParticleSystem smokeParticleSystem_;
	std::vector<ParticleSystem *> particleSystems_;

	Skybox skybox_;

	MeshModel groundMeshModel_;
	MeshModel appleMeshModel_;
	MeshModel soldierMeshModel_;
	MeshModel soldierMeshModel2_;
	std::vector<MeshModel *> meshModels_;
	
	std::chrono::high_resolution_clock::time_point startingTime_;
	GLfloat lastMouseX_;
	GLfloat lastMouseY_;
};