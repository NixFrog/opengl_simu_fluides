#pragma once

#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderProgramManager.hpp"

class ParticleBillboardingShaderProgram : public ShaderProgramManager
{
// functions
public:
	ParticleBillboardingShaderProgram();
	virtual void init();

// setters
public:
	void setModel(const glm::mat4 model);
	void setView(const glm::mat4 view);
	void setProjection(const glm::mat4 projection);
	void setSampler(unsigned int textureUnit);

private:
	GLuint projectionLocation_;
	GLuint viewLocation_;
	GLuint modelLocation_;
	GLuint samplerLocation_;
	GLuint particleSizeLocation_;
};