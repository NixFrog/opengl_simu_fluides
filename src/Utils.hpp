#pragma once

#define SAFE_DELETE(pointer) if(pointer){delete pointer; pointer = NULL;}
#define SAFE_SINGLE_BUFFER_DELETE(singleBuffer) if(singleBuffer != OPENGL_INVALID_VALUE){glDeleteBuffers(1, &singleBuffer);}
#define ZERO_MEMORY(mem) memset(mem, 0, sizeof(mem))


///////////////
// CONSTANTS //
///////////////

//Window constants
#define WIDTH 1900
#define HEIGHT 1000
#define WINDOW_MARGIN 30

//Texture constants
#define TEXTURE_UNIT_INDEX_COLOR 0

#define LOG_FILE_SIZE 1024

//Safety constants
#define OPENGL_INVALID_VALUE 0xffffffff

//Camera constants
#define CAMERA_MAX_PITCH 89.0f
#define CAMERA_MAX_ZOOM 10.0f

#define CAMERA_YAW -90.0f
#define CAMERA_PITCH 0.0f
#define CAMERA_SPEED 15.0f
#define CAMERA_SENSITIVTY 0.25f
#define CAMERA_ZOOM 45.0f

//Particle constants
#define PARTICLE_MAX_NUMBER 1000

enum Direction{
	UP,
	DOWN,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};