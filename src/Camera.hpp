#pragma once

// Standard
#include <iostream>
#include <vector>

//OpenGL
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Utils.hpp"

class Camera
{
public:
	Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = CAMERA_YAW, GLfloat pitch = CAMERA_PITCH);
	Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch);

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix(float width, float height);
	glm::vec3 getUpVector();
	glm::vec3 getPosition();
	glm::vec3 getFrontVector();
	
	/*
	* Processes a Direction given as a keyboard input, then calculates the correct position according to the camera's speed and the time elapsed.
	* @param direction a direction to move the camera towards
	* @param timeElapsed the time elapsed since the camera was last moved.
	*/
	void processKeyboardInput(Direction direction, GLfloat timeElapsed);

	/*
	* Process the mouse's movements. 
	* Offsets the camera's angles by the given values. If constrainPitch is true, then the camera is limited vertically.
	* @param xoffset horizontal offset.
	* @param yoffset vertical offset.
	* @param constrainPitch wether the camera's vertical angle should be constrained or not.
	*/
	void processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true);

	/*
	* offset zoom according to the given parameter.
	* @param yoffset zoom offset.
	*/
	void offsetZoom(GLfloat yoffset);

private:
	void updateCameraVectors();

private:
	 // Camera Attributes
	glm::vec3 position_;
	glm::vec3 front_;
	glm::vec3 up_;
	glm::vec3 right_;
	glm::vec3 worldUp_;
	// Eular Angles
	GLfloat yaw_;
	GLfloat pitch_;
	// Camera options
	GLfloat movementSpeed_;
	GLfloat mouseSensitivity_;
	GLfloat zoom_;
};