#include "Mesh.hpp"

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture *> textures){
	vertices_ = vertices;
	indices_ = indices;
	textures_ = textures;
}

void Mesh::render(){
	shaderPgm_.activate();
	shaderPgm_.setModel(model_);
	shaderPgm_.setProjection(projection_);
	shaderPgm_.setView(view_);

	for(unsigned int i=0; i<textures_.size(); i++){
		textures_[i]->bindTexture(GL_TEXTURE0 +i);
	}
	
	glBindVertexArray(vao_);

	glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	
	for(unsigned int i=0; i<textures_.size(); i++){
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Mesh::init(){
	shaderPgm_.init();

	glGenVertexArrays(1, &vao_);
	glGenBuffers(1, &vbo_);
	glGenBuffers(1, &ebo_);

	glBindVertexArray(vao_);
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo_);
	glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), &vertices_[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(GLuint), &indices_[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal_));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, textureCoordinates_));

	glBindVertexArray(0);
}

void Mesh::setModel(const glm::mat4 &model){
	model_ = model;
}

void Mesh::setProjection(const glm::mat4 &projection){
	projection_ = projection;
}

void Mesh::setView(const glm::mat4 &view){
	view_ = view;
}

void Mesh::setTextures(const std::vector<Texture *> textures){
	textures_ = textures;

	vertices_[0].textureCoordinates_ = glm::vec2(1,1);
	vertices_[1].textureCoordinates_ = glm::vec2(0,1);
	vertices_[2].textureCoordinates_ = glm::vec2(0,0);
	vertices_[3].textureCoordinates_ = glm::vec2(1,0);
	vertices_[4].textureCoordinates_ = glm::vec2(1,1);
	vertices_[5].textureCoordinates_ = glm::vec2(0,0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_);
	glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), &vertices_[0], GL_STATIC_DRAW);
}