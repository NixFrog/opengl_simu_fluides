#pragma once

#include <iostream>
#include <GL/glew.h>
#include <SOIL/SOIL.h>

#include "Utils.hpp"

class Texture
{
public:
	Texture(GLenum textureTarget, const char *textureFilePath);

	/*
	* Loads a texture from textureFilePath_ into texture_.
	*/
	void loadTexture();

	/*
	* Actives the given texture unit, and binds texture_.
	* @param textureUnit a texture unit.
	*/
	void bindTexture(GLenum textureUnit);
	const char *getTextureFilePath();

private:
	const char *textureFilePath_;
	GLuint texture_;
	GLenum textureTarget_;
	bool isLoaded_;
};