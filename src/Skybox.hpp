#pragma once

// Standard
#include <iostream>
#include <vector>

//OpenGL
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <SOIL/SOIL.h>

#include "Texture.hpp"
#include "SkyboxShaderProgram.hpp"

class Skybox
{
public:
	Skybox();
	~Skybox();

	void init();
	void render();

	void setView(glm::mat4 viewMatrix);
	void setProjection(glm::mat4 projectionMatrix);

private:
	/*
	* Loads a cubemap from faces_.
	*/
	GLuint loadCubemap();

private:
	SkyboxShaderProgram shaderPgm_;
	GLuint vao_;
	GLuint vbo_;

	GLuint cubeMapTexture_;
	std::vector<const GLchar*> faces_;

	glm::mat4 view_;
	glm::mat4 projection_;

	bool isInitialized_;
};