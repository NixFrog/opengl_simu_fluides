#include "Camera.hpp"

Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch): 
	front_(glm::vec3(0.0f, 0.0f, -1.0f)),
	movementSpeed_(CAMERA_SPEED),
	mouseSensitivity_(CAMERA_SENSITIVTY),
	zoom_(CAMERA_ZOOM)
{
	position_ = position;
	worldUp_  = up;
	yaw_      = yaw;
	pitch_    = pitch;
	updateCameraVectors();
}

Camera::Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) :
	front_(glm::vec3(0.0f, 0.0f, -1.0f)),
	movementSpeed_(CAMERA_SPEED),
	mouseSensitivity_(CAMERA_SENSITIVTY),
	zoom_(CAMERA_ZOOM)
{
	position_ = glm::vec3(posX, posY, posZ);
	worldUp_  = glm::vec3(upX, upY, upZ);
	yaw_      = yaw;
	pitch_    = pitch;
	updateCameraVectors();
}

glm::mat4 Camera::getViewMatrix(){
	return glm::lookAt(position_, position_ + front_, up_);
}

void Camera::processKeyboardInput(Direction direction, GLfloat timeElapsed){
	GLfloat dist = movementSpeed_ * timeElapsed;
	if(direction == FORWARD){
		position_ += front_ * dist;
	}
	else if(direction == BACKWARD){
		position_ -= front_ * dist;
	}
	else if(direction == RIGHT){
		position_ += right_ * dist;
	}
	else if(direction == LEFT){
		position_ -= right_ *dist;
	}
	else if(direction == UP){
		position_ += up_ * dist;
	}
	else if(direction == DOWN){
		position_ -= up_ *dist;
	}
}

void Camera::processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch){
	xoffset *= mouseSensitivity_;
	yoffset *= mouseSensitivity_;
	yaw_    += xoffset;
	pitch_  -= yoffset;

	if(constrainPitch){
		if (pitch_ > CAMERA_MAX_PITCH){
			pitch_ = CAMERA_MAX_PITCH;
		}
		if (pitch_ < -CAMERA_MAX_PITCH){
			pitch_ = -CAMERA_MAX_PITCH;
		}
	}

	updateCameraVectors();
}

void Camera::offsetZoom(GLfloat yoffset){
	if(zoom_ >= 1.0f && zoom_ <= CAMERA_MAX_ZOOM){
		zoom_ -= yoffset;
	}
	if(zoom_ < 1.0f){
		zoom_ = 1.0f;
	}
	else if(zoom_ > CAMERA_MAX_ZOOM){
		zoom_ = CAMERA_MAX_ZOOM;
	}
}

void Camera::updateCameraVectors(){
	glm::vec3 frontVector;
	
	frontVector.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
	frontVector.y = sin(glm::radians(pitch_));
	frontVector.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));
	
	front_ = glm::normalize(frontVector);
	right_ = glm::normalize(glm::cross(front_, worldUp_));
	up_    = glm::normalize(glm::cross(right_, front_));
}

glm::mat4 Camera::getProjectionMatrix(float width, float height){
	glm::mat4 projection = glm::perspective(zoom_, (float)width/(float)height, 0.1f, 1000.0f);
	return projection;
}

glm::vec3 Camera::getUpVector(){
	return up_;
}

glm::vec3 Camera::getPosition(){
	return position_;
}

glm::vec3 Camera::getFrontVector(){
	return front_;
}