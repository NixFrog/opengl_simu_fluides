#include "ShaderProgramManager.hpp"

ShaderProgramManager::ShaderProgramManager(){
	shaderProgram_ = 0;
	isInitialized_ = false;
}

ShaderProgramManager::~ShaderProgramManager(){
	shadersVector_.clear();

	glDeleteProgram(shaderProgram_);
	shaderProgram_ = 0;
}

void ShaderProgramManager::init(){
	if(isInitialized_){
		std::cerr<<"Warning: several initializations of a single ShaderProgramManager instance"<<std::endl;
	}
	shaderProgram_ = glCreateProgram();

	if(shaderProgram_ == 0) {
		std::cerr<<"Error creating shader program"<<std::endl;
		exit(EXIT_FAILURE);
	}
}

void ShaderProgramManager::addShader(GLenum type, std::string shaderFilePath){
	std::string shaderString;
	shaderString = readTextFile(shaderFilePath);
	GLuint shader = glCreateShader(type);

	if(shader == 0) {
		std::cerr<<"Error creating shader type: "<<type<<std::endl;
		exit(EXIT_FAILURE);
	}
	
	shadersVector_.push_back(shader);

	GLchar const* files[] = {shaderString.c_str()};
	GLint lengthSum[1] = {(GLint)shaderString.size()};

	glShaderSource(shader, 1, files, lengthSum);
	glCompileShader(shader);

	std::string logFileName = findLogFileFromPath(shaderFilePath);
	checkShaderCompilation(shader, logFileName);

	glAttachShader(shaderProgram_, shader);
}

std::string ShaderProgramManager::readTextFile(std::string filePath){
	std::ifstream ifs(filePath);
	std::string content;
	content.assign((std::istreambuf_iterator<char>(ifs)),
					std::istreambuf_iterator<char>());

	return content;
}

std::string ShaderProgramManager::findLogFileFromPath(std::string filePath){
	std::string logFileName = "";
	size_t posName = filePath.find_last_of("/");
	size_t posExt = filePath.find_last_of(".");
	logFileName = filePath.substr(posName+1, posExt-posName-1);
	logFileName.append(".log");
	return logFileName;
}

void ShaderProgramManager::writeCompilationResultsInFile(char *buffer, std::string logFilePath){
	std::ofstream logFile;
	logFile.open(logFilePath, std::ofstream::out | std::ofstream::trunc);
	logFile << buffer << std::endl;
	logFile.close();
}

void ShaderProgramManager::checkShaderCompilation(GLuint shader, std::string logFileName){
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if(status != GL_TRUE){
	    std::cerr << "Failed to compile shader "<<logFileName.substr(0, logFileName.size()-4)<<". Check logs for more infos." << std::endl;	
	    exit(EXIT_FAILURE);
	}
	char vLogBuffer[LOG_FILE_SIZE];
	glGetShaderInfoLog(shader, LOG_FILE_SIZE, NULL, vLogBuffer);
	
	std::string logFilePath ="./logs/";
	logFilePath.append(logFileName);
	writeCompilationResultsInFile(vLogBuffer, logFilePath);
}

void ShaderProgramManager::checkShaderProgramLinking(std::string logFileName){
	GLint status;
	glGetProgramiv(shaderProgram_, GL_LINK_STATUS, &status);
	if(status != GL_TRUE){
		std::cerr<<"Error linking shader program. Check logs for more infos." << std::endl;
		exit(EXIT_FAILURE);
	}
	char vLogBuffer[LOG_FILE_SIZE];
	glGetProgramInfoLog(shaderProgram_, LOG_FILE_SIZE, NULL, vLogBuffer);

	std::string logFilePath ="./logs/";
	logFilePath.append(logFileName);
	writeCompilationResultsInFile(vLogBuffer, logFilePath);
}

void ShaderProgramManager::checkShaderProgramStatus(std::string logFileName){
	GLint status;
	glGetProgramiv(shaderProgram_, GL_VALIDATE_STATUS, &status);
	if(status != GL_TRUE){
		std::cerr<<"Invalid shader program. Check logs for more infos." << std::endl;
		exit(EXIT_FAILURE);
	}
	char vLogBuffer[LOG_FILE_SIZE];
	glGetProgramInfoLog(shaderProgram_, LOG_FILE_SIZE, NULL, vLogBuffer);

	std::string logFilePath ="./logs/";
	logFilePath.append(logFileName);
	writeCompilationResultsInFile(vLogBuffer, logFilePath);
}

void ShaderProgramManager::completeInitialization(){
	glLinkProgram(shaderProgram_);
	checkShaderProgramLinking("shader_programs.log");

	glValidateProgram(shaderProgram_);
	checkShaderProgramStatus("shader_programs.log");

	for (std::vector<GLuint>::iterator it = shadersVector_.begin() ; it != shadersVector_.end() ; it++) {
		glDeleteShader(*it);
	}

	shadersVector_.clear();
	isInitialized_ = true;
}

void ShaderProgramManager::activate(){
	if(!isInitialized_){
		std::cerr<<"Warning: activating an unitialized ShaderProgramManager instance"<<std::endl;
	}
	glUseProgram(shaderProgram_);
}

GLint ShaderProgramManager::getUniformLocation(const char* uniformName){
	GLuint uniformLocation = glGetUniformLocation(shaderProgram_, uniformName);

	if (uniformLocation == 0xffffffff) {
		std::cerr<<"WARNING: could not get the location of uniform \""<<uniformName<<"\""<<std::endl;
	}

	return uniformLocation;
}

GLint ShaderProgramManager::getShaderProgramParameter(GLint parameter){
	GLint returnValue;
	glGetProgramiv(shaderProgram_, parameter, &returnValue);
	return returnValue;
}
