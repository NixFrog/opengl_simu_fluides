#pragma once

#include <iostream>
#include <GLFW/glfw3.h>

#include "Utils.hpp"

class WindowCreator
{	
public:

	WindowCreator();

	/*
	* Create a GLFW window in windowed mode
	* @param title window title
	* @param width window width at creation
	* @param height window height at creation
	* @return the glfw window
	*/
	GLFWwindow *createWindow(char const *title);

private:
	/*
	* Callback function for GLFW errors
	*/
	void static errorCallback(int error, const char *errorDescription);

	/*
	* Callback function for GLFW key inputs
	*/
	void static keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
};