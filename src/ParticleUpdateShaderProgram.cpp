#include "ParticleUpdateShaderProgram.hpp"

ParticleUpdateShaderProgram::ParticleUpdateShaderProgram(){}

void ParticleUpdateShaderProgram::init(){
	ShaderProgramManager::init();
	addShader(GL_VERTEX_SHADER, 	"./src/shaders/ParticleShaders/particleUpdate_vshader.vs");
	addShader(GL_GEOMETRY_SHADER, 	"./src/shaders/ParticleShaders/particleUpdate_gshader.gs");
	
	const GLchar* feedbackVaryingsArray[NB_PARTICLE_ATTRIBUTES] = 
	{
		"vPositionOut",
		"vVelocityOut",
		"vColorOut",
		"fLifeTimeOut",
		"fSizeOut",
		"iTypeOut",
	};

	for(int i=0; i < NB_PARTICLE_ATTRIBUTES; i++){
		glTransformFeedbackVaryings(shaderProgram_, NB_PARTICLE_ATTRIBUTES, feedbackVaryingsArray, GL_INTERLEAVED_ATTRIBS);
	}
	
	completeInitialization();

	particleGeneratorPositionLocation_        = getUniformLocation("vGenPosition");
	generatedParticleGravityVectorLocation_   = getUniformLocation("vGenGravityVector");
	generatedParticleMinimumVelocityLocation_ = getUniformLocation("vGenVelocityMin");
	generatedParticleVelocityRangeLocation_   = getUniformLocation("vGenVelocityRange");
	generatedParticleColorLocation_           = getUniformLocation("vColor");
	generatedParticleSizeLocation_            = getUniformLocation("fParticleSize");
	generatedParticleMinimumLifeLocation_     = getUniformLocation("fLifetimeMin");
	generatedParticleLifeRangeLocation_       = getUniformLocation("fLifetimeRange");
	timePassedLocation_                       = getUniformLocation("fTimePassed");
	numberOfParticlesToGenerateLocation_      = getUniformLocation("iNumToGenerate");
	randomSeedLocation_                       = getUniformLocation("vRandomSeed");
	floorPositionLocation_                    = getUniformLocation("fFloorPosition");

}

int ParticleUpdateShaderProgram::getSizeOfFeedbackVaryingsArray(){
	return NB_PARTICLE_ATTRIBUTES;
}

void ParticleUpdateShaderProgram::setParticleGeneratorPosition(const glm::vec3 generatorParticle){
	glUniform3f(particleGeneratorPositionLocation_, generatorParticle.x, generatorParticle.y, generatorParticle.z);
}

void ParticleUpdateShaderProgram::setGeneratedParticleGravityVector(const glm::vec3 generatedParticleGravityVector){
	glUniform3f(generatedParticleGravityVectorLocation_, generatedParticleGravityVector.x, generatedParticleGravityVector.y, generatedParticleGravityVector.z);
}

void ParticleUpdateShaderProgram::setGeneratedParticleMinimumVelocity(const glm::vec3 generatedParticleMinimumVeolocity){
	glUniform3f(generatedParticleMinimumVelocityLocation_, generatedParticleMinimumVeolocity.x, generatedParticleMinimumVeolocity.y, generatedParticleMinimumVeolocity.z);
}

void ParticleUpdateShaderProgram::setGeneratedParticleVelocityRange(const glm::vec3 generatedParticleVelocityRange){
	glUniform3f(generatedParticleVelocityRangeLocation_, generatedParticleVelocityRange.x, generatedParticleVelocityRange.y, generatedParticleVelocityRange.z);
}

void ParticleUpdateShaderProgram::setGeneratedParticleColor(const glm::vec3 generatedParticleColor){
	glUniform3f(generatedParticleColorLocation_, generatedParticleColor.x, generatedParticleColor.y, generatedParticleColor.z);
}

void ParticleUpdateShaderProgram::setGeneratedParticleSize(float generatedParticleSize){
	glUniform1f(generatedParticleSizeLocation_, generatedParticleSize);
}

void ParticleUpdateShaderProgram::setGeneratedParticleMinimumLife(float generatedParticleMinimumLife){
	glUniform1f(generatedParticleMinimumLifeLocation_, generatedParticleMinimumLife);
}

void ParticleUpdateShaderProgram::setGeneratedParticleLifeRange(float generatedParticleLifeRange){
	glUniform1f(generatedParticleLifeRangeLocation_, generatedParticleLifeRange);
}

void ParticleUpdateShaderProgram::setTimePassed(float timePassed){
	glUniform1f(timePassedLocation_, timePassed);
}

void ParticleUpdateShaderProgram::setNumberOfParticlesToGenerate(int numberOfParticlesToGenerate){
	glUniform1i(numberOfParticlesToGenerateLocation_, numberOfParticlesToGenerate);
}

void ParticleUpdateShaderProgram::setRandomSeed(const glm::vec3 randomSeed){
	glUniform3f(randomSeedLocation_, randomSeed.x, randomSeed.y, randomSeed.z);
}

void ParticleUpdateShaderProgram::setFloorPosition(float floorPosition){
	glUniform1f(floorPositionLocation_, floorPosition);
}