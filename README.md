# README #

## Dependencies

* [premake4](https://premake.github.io/)
* [GLFW](http://www.glfw.org/)
* [GLEW](http://glew.sourceforge.net/)
* [GLM](http://glm.g-truc.net/0.9.7/index.html)
* [SOIL](http://www.lonesock.net/soil.html)
* [Assimp](https://github.com/assimp/assimp)

## Build instructions

```
> premake4 gmake
> make
> ./3D_fluid_simulation
```


## Controls

| Inputs                        | Actions                                      |
|-------------------------------|----------------------------------------------|
| up                            | move camera forward                          |
| down                          | move camera backward                         |
| left                          | rotate camera left                           |
| right                         | rotate camera right                          |
| hold left click               | move camera                                  |
| ZQSDAW                        | change the fountain's initial direction      |

### Contribution guidelines ###

See the Wiki

## References

### Tutorials & Resources

* [Learn OpenGL](http://learnopengl.com/)
* [OGLdev](http://ogldev.atspace.co.uk/)
* [Open.GL](https://open.gl/)