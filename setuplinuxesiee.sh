#!/bin/bash


if [ $# -ne 1 ]
	then
		echo "Incorrect use: script <git https clone adress>"
		exit 1
fi

http_proxy=cache.esiee.fr:3128
export http_proxy

git config --global http.proxy cache.esiee.fr:3128
git clone $1

sudo apt-get install premake4
sudo apt-get install libsoil-dev
sudo apt-get install libglew-dev
sudo apt-get install libglfw3-dev
sudo apt-get install freeglut3-dev



exit 0
