solution "PR-4104"
	configurations ""
	project "3D_fluid_simulation"
	kind "ConsoleApp"
	language "C++"
	files({ "src/*.h", "src/*.cpp" })
	--includedirs({"/", "includes"})

	flags({"Symbols", "ExtraWarnings"})
	links({})
	--libdirs({"Driver/"})

	buildoptions({"-std=c++11","-Wextra", "-Os", "-s", "-DFREEGLUT_STATIC", "-DGLEW_STATIC"})
	linkoptions({"-std=c++11", "-lpthread", "-lm", "-lGL", "-lSOIL", "-L/usr/X11R6/lib", "-lGLU", "-lglut", "-lGLEW", "-lglfw", "-lassimp"})
